package platformbuilders.io;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import platformbuilders.io.repository.ClienteRepositoryTest;

@ExtendWith(SpringExtension.class)
@RunWith(Suite.class)
@SuiteClasses({
	ClienteRepositoryTest.class,
})
public class SuiteGeneralTests {

}
