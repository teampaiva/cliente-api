package platformbuilders.io.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import platformbuilders.io.repository.ClienteRepository;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ClienteController.class)
@ActiveProfiles("test")
public class ClienteControllerTest {
	
	@Autowired
    MockMvc mvc;

    @MockBean
    ClienteRepository clienteRepository;
    
    @Test
    void teste_0001() {
    	assertThat(clienteRepository).isNotNull();
    }

}
